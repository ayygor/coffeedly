export const CREATE_NOTE_ACTION = 'createNoteAction'
export const GET_NOTES_ACTION = 'getNotesAction'
export const CREATE_PHOTO_ACTION = 'createPhotoAction'
export const GET_PHOTOS_ACTION = 'getPhotosAction'
export const CREATE_RECIPE_ACTION = 'createRecipeAction'
export const GET_RECIPES_ACTION = 'getRecipesAction'
export const ADD_LIKE_ACTION = 'addLikeAction'
export const REMOVE_LIKE_ACTION = 'removeLikeAction'
