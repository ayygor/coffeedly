import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/messaging'

const config = {
  apiKey: 'AIzaSyD1ZMtj-1XAAB1bOv0DAfjqugF6m5-jClI',
  authDomain: 'coffeedly.firebaseapp.com',
  databaseURL: 'https://coffeedly.firebaseio.com',
  projectId: 'coffeedly',
  storageBucket: 'coffeedly.appspot.com',
  messagingSenderId: '935988452605',
  appId: '1:935988452605:web:6dd858f98cd88201383294'
}

firebase.initializeApp(config)

const firestore = firebase.firestore()
const settings = { timestampsInSnapshots: true }
firestore.settings(settings)
const db = firestore
export default {
  db
}
