export const FIREBASE_COLLECTION_NOTES = 'notes'
export const FIREBASE_COLLECTION_PHOTOS = 'photos'
export const FIREBASE_COLLECTION_RECIPES = 'recipes'
export const FIREBASE_COLLECTION_USERS = 'users'
