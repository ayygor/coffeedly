export const backButtonMixin = {
  data() {
    return { dialogOpen: false }
  },
  mounted() {
    if (this.$device.isMobileOrTablet) {
      this.$root.$on('dialogOpened', () => {
        this.dialogOpen = true
      })
      this.$root.$on('dialogClosed', () => {
        this.dialogOpen = false
      })
    }
  },
  beforeRouteLeave(to, from, next) {
    if (this.$device.isMobileOrTablet) {
      if (this.dialogOpen) {
        this.$root.$emit('closeDialog', {})
        next(false)
      } else {
        next()
      }
    } else {
      next()
    }
  },
  beforeDestroy() {
    // always remember to unsubscribe
    if (this.$device.isMobileOrTablet) {
      this.$root.$off('dialogOpen')
      this.$root.$off('dialogClosed')
    }
  }
}
