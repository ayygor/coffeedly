// export default {
module.exports = {
  mode: 'spa',
  // buildDir: 'functions/.nuxt',
  // srcDir: '.',
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  pwa: {
    manifest: {
      name: 'Coffeedly',
      lang: 'en'
    },
    meta: {
      name: 'Coffeedly',
      nativeUI: true,
      theme_color: '#ffffff'
    },
    workbox: {
      importScripts: [
        '/firebase-auth-sw.js'
      ],
      runtimeCaching: [
        {
          // Should be a regex string. Compiles into new RegExp('https://my-cdn.com/.*')
          urlPattern: '/api/(.*)'
          // Defaults to `networkFirst` if omitted
          // handler: 'networkFirst',
          // Defaults to `GET` if omitted
          // method: 'GET'
        }
      ]
    }
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#43342c' },
  /*
   ** Global CSS
   */
  css: ['@/assets/scss/home.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    // { src: '~/plugins/infiniteloading', ssr: false },
    { src: '~/plugins/touchevents', ssr: false },
    '@/plugins/axios',
    '@/plugins/services/firestore-service'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://buefy.github.io/#/documentation
    'nuxt-buefy',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    'nuxt-fullpage.js',
    'nuxt-fontawesome',
    // '@bazzite/nuxt-optimized-images',
    'vue-web-cam/nuxt',
    '@nuxtjs/device',
    [
      '@nuxtjs/firebase',
      {
        config: {
          apiKey: 'AIzaSyD1ZMtj-1XAAB1bOv0DAfjqugF6m5-jClI',
          authDomain: 'coffeedly.firebaseapp.com',
          databaseURL: 'https://coffeedly.firebaseio.com',
          projectId: 'coffeedly',
          storageBucket: 'coffeedly.appspot.com',
          messagingSenderId: '935988452605',
          appId: '1:935988452605:web:06c93b192289fca5383294'
        },
        services: {
          auth: true,
          firestore: true,
          storage: true
        }
      }
    ]
  ],
  optimizedImages: {
    optimizeImages: true
  },
  buefy: {
    materialDesignIcons: false,
    defaultIconPack: 'fas'
  },
  /*
   ** Axios module configuration]
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    // baseURL: 'http://192.168.43.198:5000'
    //  baseURL: 'http://192.168.1.101:5000'
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extractCSS: true,
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true
          }
        })
      }
    }
  }
}
