import { FIREBASE_COLLECTION_USERS } from '~/const-types'
export const COLLECTION_USERS = 'users'

export const state = () => ({
  username: '',
  email: '',
  uid: '',
  photo: ''
})

export const mutations = {
  setUser(state, payload) {
    state.username = payload.username
    state.uid = payload.uid
    state.email = payload.email
    state.photo = payload.photo
  }
}

export const actions = {
  async logout() {
    localStorage.clear()
    await this.$router.push('/')
  },
  async login({ commit }, user) {
    await this.$fireAuth
      .signInWithEmailAndPassword(user.email, user.password)
      .then((result) =>
        commit('setUser', {
          username: user.username,
          uid: result.user.uid,
          email: result.user.email
        })
      )
  },
  async createUser({ dispatch, commit }, user) {
    await this.$fireAuth
      .createUserWithEmailAndPassword(user.email, user.password)
      .then((result) => {
        this.$fireStore
          .collection(COLLECTION_USERS)
          .doc(result.user.uid)
          .set({
            email: result.user.email,
            uid: result.user.uid,
            username: user.username
          })

        dispatch('login', user)
      })
  },
  loginProviders({ commit }, provider) {
    this.$fireAuth.signInWithPopup(provider).then((result) => {
      this.$fireStore
        .collection(COLLECTION_USERS)
        .doc(result.user.uid)
        .set({
          email: result.user.email,
          uid: result.user.uid,
          username: result.user.displayName,
          photo: result.user.photoURL
        })
      commit('setUser', {
        username: result.user.displayName,
        uid: result.user.uid,
        email: result.user.email,
        photo: result.user.photoURL
      })
    })
  },
  setUserByFirebaseAuth({ commit }, user) {
    commit('setUser', {
      username: user.displayName,
      uid: user.uid,
      email: user.email,
      photo: user.photoURL
    })
  },
  async getUserFromFireStore({ commit }, { user }) {
    await this.$fireService.getDocumentFromCollection(this.$fireStore, {
      collection: FIREBASE_COLLECTION_USERS,
      user
    })
  }
}
