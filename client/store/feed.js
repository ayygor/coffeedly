export const state = () => ({
  recipes: { recipeList: [], lastDoc: null, eof: false },
  photos: { photoList: [], lastDoc: null, eof: false }
}) // XXX: refactor this to separate modules

export const mutations = {
  setRecipes(state, recipes) {
    state.recipes.recipeList = state.recipes.recipeList.concat(recipes)
    state.recipes.page++
  },
  setPhotos(state, photos) {
    state.photos.photoList = state.photos.photoList.concat(photos)
    state.photos.page++
  },
  addRecipe(state, recipe) {
    state.recipes.recipeList = [recipe].concat(state.recipes.recipeList)
  },
  addPhoto(state, photo) {
    state.photos.photoList = [photo].concat(state.photos.photoList)
  }
}
export const actions = {
  async getRecipes({ commit }) {
    try {
      const data = []
      await this.$fireStore
        .collection('recipes')
        .orderBy('datetime')
        .get()
        .then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            const t = doc.data()
            t.id = doc.id
            data.push(t)
          })
        })
      commit('setRecipes', data)
    } catch (e) {
      console.log(e)
    }
  },
  async getPhotos({ commit }) {
    try {
      const data = []
      await this.$fireStore
        .collection('photos')
        .orderBy('datetime')
        .get()
        .then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            const t = doc.data()
            t.id = doc.id
            data.push(t)
          })
        })
      commit('setPhotos', data)
    } catch (e) {
      console.log(e)
    }
  },

  async show({ commit }, params) {
    //
  },
  async set({ commit }, post) {
    await commit('set', post)
  }
}
