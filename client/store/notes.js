import {
  ADD_LIKE_ACTION,
  CREATE_NOTE_ACTION,
  GET_NOTES_ACTION,
  REMOVE_LIKE_ACTION
} from '~/action-types'
import {
  FIREBASE_COLLECTION_NOTES,
  FIREBASE_COLLECTION_USERS
} from '~/const-types'
import {
  ADD_NOTE_MUTATION,
  SET_NOTES_MUTATION,
  UPDATE_NOTE_LIKE_COUNT
} from '~/mutation-types'

export const state = () => ({
  notes: [],
  lastDoc: null,
  loading: false
})

export const mutations = {
  [ADD_NOTE_MUTATION](state, payload) {
    state.notes.unshift(payload)
  },
  [SET_NOTES_MUTATION](state, list) {
    state.notes = list
  },
  [UPDATE_NOTE_LIKE_COUNT](state, { post, value }) {
    const item = state.notes.findIndex((item) => item.id === post.id)
    state.notes[item].likedByUser = value > 0
    state.notes[item].likes += value
  },
  _setLastDoc(state, doc) {
    state.lastDoc = doc
  }
}

export const actions = {
  async [CREATE_NOTE_ACTION]({ commit, dispatch, state }, payload) {
    try {
      const docRef = await this.$fireService.addDocumentToCollection({
        collection: FIREBASE_COLLECTION_NOTES,
        payload
      })
      const user = await this.$fireService.getDocumentFromCollection({
        collection: FIREBASE_COLLECTION_USERS,
        document: { uid: payload.user_uid }
      })
      const doc = {
        id: docRef.id,
        likes: 0,
        likedByUSer: false,
        user,
        ...payload
      }
      commit(ADD_NOTE_MUTATION, doc)
    } catch {}
  },

  async [GET_NOTES_ACTION]({ commit, dispatch, state }, { loader, user }) {
    try {
      if (state.loading) {
        return
      }
      state.loading = true
      await this.$fireService
        .getDocumentsFromCollection({
          collection: FIREBASE_COLLECTION_NOTES,
          user,
          lastDoc: state.lastDoc
        })
        .then(({ results, lastDoc }) => {
          if (results.length <= 0) {
            loader.complete()
          }
          commit('_setLastDoc', lastDoc)
          commit(SET_NOTES_MUTATION, results)
        })

      loader.loaded()
      state.loading = false
    } catch (e) {
      console.log(e)
    }
  },
  async [ADD_LIKE_ACTION]({ commit }, { post, user }) {
    try {
      await this.$fireStore
        .collection(FIREBASE_COLLECTION_NOTES)
        .doc(post.id)
        .collection('likedBy')
        .doc(user.uid)
        .set({ like: true })
        .then(() => commit(UPDATE_NOTE_LIKE_COUNT, { post, value: 1 }))
    } catch (e) {
      console.log(e)
    }
  },
  async [REMOVE_LIKE_ACTION]({ commit }, { post, user }) {
    try {
      await this.$fireStore
        .collection(FIREBASE_COLLECTION_NOTES)
        .doc(post.id)
        .collection('likedBy')
        .doc(user.uid)
        .delete()
        .then(() => commit(UPDATE_NOTE_LIKE_COUNT, { post, value: -1 }))
    } catch (e) {
      console.log(e)
    }
  }
}
