import {
  ADD_LIKE_ACTION,
  CREATE_PHOTO_ACTION,
  GET_PHOTOS_ACTION,
  REMOVE_LIKE_ACTION
} from '~/action-types'
import {
  FIREBASE_COLLECTION_PHOTOS,
  FIREBASE_COLLECTION_USERS
} from '~/const-types'
import {
  ADD_PHOTO_MUTATION,
  SET_PHOTOS_MUTATION,
  UPDATE_PHOTO_LIKE_COUNT
} from '~/mutation-types'

export const state = () => ({
  photos: [],
  lastDoc: null,
  loading: false
})

export const mutations = {
  [ADD_PHOTO_MUTATION](state, payload) {
    state.photos.unshift(payload)
  },
  [SET_PHOTOS_MUTATION](state, list) {
    state.photos = list
  },
  [UPDATE_PHOTO_LIKE_COUNT](state, { photo, value }) {
    const item = state.photos.findIndex((item) => item.id === photo.id)
    state.photos[item].likedByUser = value > 0
    state.photos[item].likes += value
  },
  _setLastDoc(state, doc) {
    state.lastDoc = doc
  }
}

export const actions = {
  async [CREATE_PHOTO_ACTION]({ commit, dispatch, state }, payload) {
    try {
      const docRef = await this.$fireService.addDocumentToCollection({
        collection: FIREBASE_COLLECTION_PHOTOS,
        payload
      })
      const user = await this.$fireService.getDocumentFromCollection({
        collection: FIREBASE_COLLECTION_USERS,
        document: { uid: payload.user_uid }
      })
      const doc = {
        id: docRef.id,
        likes: 0,
        likedByUSer: false,
        user,
        ...payload
      }
      commit(ADD_PHOTO_MUTATION, doc)
    } catch {}
  },

  async [GET_PHOTOS_ACTION]({ commit, dispatch, state }, { loader, user }) {
    try {
      if (state.loading) {
        return
      }
      state.loading = true
      await this.$fireService
        .getDocumentsFromCollection({
          collection: FIREBASE_COLLECTION_PHOTOS,
          user,
          lastDoc: state.lastDoc
        })
        .then(({ results, lastDoc }) => {
          if (results.length <= 0) {
            loader.complete()
          }
          commit('_setLastDoc', lastDoc)
          commit(SET_PHOTOS_MUTATION, results)
        })

      loader.loaded()
      state.loading = false
    } catch (e) {
      console.log(e)
    }
  },
  async [ADD_LIKE_ACTION]({ commit }, { photo, user }) {
    try {
      await this.$fireStore
        .collection(FIREBASE_COLLECTION_PHOTOS)
        .doc(photo.id)
        .collection('likedBy')
        .doc(user.uid)
        .set({ like: true })
        .then(() => commit(UPDATE_PHOTO_LIKE_COUNT, { photo, value: 1 }))
    } catch (e) {
      console.log(e)
    }
  },
  async [REMOVE_LIKE_ACTION]({ commit }, { photo, user }) {
    try {
      await this.$fireStore
        .collection(FIREBASE_COLLECTION_PHOTOS)
        .doc(photo.id)
        .collection('likedBy')
        .doc(user.uid)
        .delete()
        .then(() => commit(UPDATE_PHOTO_LIKE_COUNT, { photo, value: -1 }))
    } catch (e) {
      console.log(e)
    }
  }
}
