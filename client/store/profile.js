export const state = () => ({
  uid: '',
  username: '',
  photo: '',
  posts: [],
  recipes: [],
  photos: [],
  loading: false
})

export const mutations = {
  setPosts(state, payload) {
    state.posts = payload
  },
  setRecipes(state, payload) {
    state.recipes = payload
  },
  setPhotos(state, payload) {
    state.photos = payload
  }
}

export const actions = {
  async getPosts({ commit, state }) {
    try {
      const data = []
      const query = this.$fireStore
        .collection('notes')
        .where('user_uid', '==', state.uid)
        // .orderBy('datetime', 'desc')
        .limit(10)

      await query.get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          const t = doc.data()
          t.id = doc.id
          data.push(t)
        })
      })
      commit('setPosts', data)
    } catch (e) {
      console.log(e)
    }
  },
  async getRecipes({ commit, state }) {
    try {
      const data = []
      await this.$fireStore
        .collection('recipes')
        .where('user_uid', '==', state.uid)
        // .orderBy('datetime')
        .get()
        .then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            const t = doc.data()
            t.id = doc.id
            data.push(t)
          })
        })
      commit('setRecipes', data)
    } catch (e) {
      console.log(e)
    }
  },
  async getPhotos({ commit, state }) {
    try {
      const data = []
      await this.$fireStore
        .collection('photos')
        .where('user_uid', '==', state.uid)
        // .orderBy('datetime')
        .get()
        .then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            const t = doc.data()
            t.id = doc.id
            data.push(t)
          })
        })
      commit('setPhotos', data)
    } catch (e) {
      console.log(e)
    }
  },
  async updateUserProfile({ dispatch, commit, state }, payload) {
    state.uid = payload.user.uid
    state.username = payload.user.username
    state.photo = payload.user.photo
    const promises = [await dispatch('getRecipes')]

    Promise.allSettled(promises)
  }
}
